sudo apt update -y 

sudo apt install ros-melodic-moveit -y
sudo apt-get install ros-melodic-ros-control ros-melodic-ros-controllers -y
sudo apt install ros-melodic-tf2-web-republisher -y
sudo apt install ros-melodic-rosbridge-server -y
sudo apt install ros-melodic-joint-state-publisher-gui -y 
sudo apt install build-essential -y 
sudo apt install python-catkin-pkg -y
sudo apt install python-pymodbus -y 
sudo apt install python-rosdistro -y
sudo apt install python-rospkg -y 
sudo apt install python-rosdep-modules -y
sudo apt install python-rosinstall -y
sudo apt install python-rosinstall-generator -y
sudo apt install python-wstool -y

cd ..
sudo rosdep fix-permissions
rosdep update
rosdep install --from-paths src --ignore-src --default-yes --rosdistro melodic --skip-keys "python-rpi.gpio"
cd ${RIDE_SOURCE_PATH}
mv camera.launch ${RIDE_SOURCE_PATH}/_lib/ned_ros/niryo_robot_bringup/launch
cd ${RIDE_SOURCE_PATH}/_lib/ned_ros/niryo_robot_bringup/launch
git apply ${RIDE_SOURCE_PATH}/without_cam.patch
cd ${RIDE_SOURCE_PATH}/..

chmod +x ${RIDE_SOURCE_PATH}/_lib/ned_ros/niryo_robot_arm_commander/scripts/arm_commander.py
chmod +x ${RIDE_SOURCE_PATH}/_lib/ned_ros/niryo_robot_tools_commander/scripts/tool_commander_node.py
chmod +x ${RIDE_SOURCE_PATH}/_lib/ned_ros/niryo_robot_poses_handlers/scripts/poses_handlers_node.py
chmod +x ${RIDE_SOURCE_PATH}/_lib/ned_ros/niryo_robot_programs_manager/scripts/programs_manager_node.py
chmod +x ${RIDE_SOURCE_PATH}/_lib/ned_ros/niryo_robot_user_interface/scripts/user_interface_node.py
chmod +x ${RIDE_SOURCE_PATH}/_lib/ned_ros/niryo_robot_modbus/scripts/modbus_server_node.py
chmod +x ${RIDE_SOURCE_PATH}/_lib/ned_ros/niryo_robot_rpi/scripts/fake_rpi_node.py
chmod +x ${RIDE_SOURCE_PATH}/_lib/ned_ros/niryo_robot_arm_commander/scripts/robot_commander_node.py

catkin_make
source devel/setup.bash
